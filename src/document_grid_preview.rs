/* list_item.rs
 *
 * Copyright 2023 Justus Hecker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use glib::Object;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, pango, Box, Label, Orientation};
use std::cell::OnceCell;
use std::fs;

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct DocumentGridPreview {
        pub title: OnceCell<gtk::Label>,
        pub body: OnceCell<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DocumentGridPreview {
        const NAME: &'static str = "SentenceDocumentGridPreview";
        type Type = super::DocumentGridPreview;
        type ParentType = gtk::Button;
    }

    impl ObjectImpl for DocumentGridPreview {
        fn constructed(&self) {
            let obj = self.obj();
            // obj.imp().title
            obj.setup_layout(Some("Test"));
        }
    }

    impl WidgetImpl for DocumentGridPreview {}

    impl ButtonImpl for DocumentGridPreview {}
}

glib::wrapper! {
    pub struct DocumentGridPreview(ObjectSubclass<imp::DocumentGridPreview>)
        @extends gtk::Button, gtk::Widget,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget, gtk::Scrollable, gio::ActionMap;
}

impl Default for DocumentGridPreview {
    fn default() -> Self {
        Self::new()
    }
}

impl DocumentGridPreview {
    pub fn new() -> Self {
        Object::builder().build()
    }

    fn setup_layout(&self, label_text: Option<&str>) {
        let grid_box = Box::new(Orientation::Vertical, 6);
        let title = Label::new(label_text);
        title.set_css_classes(&["heading", "bold"]);
        title.set_ellipsize(pango::EllipsizeMode::End);
        title.set_wrap(true);
        title.set_properties(&[
            ("margin-start", &12),
            ("margin-end", &12),
            ("margin-top", &12),
            ("margin-bottom", &12),
        ]);
        grid_box.append(&title);
        self.imp().title.set(title).unwrap();

        let body = Label::new(None);
        body.set_ellipsize(pango::EllipsizeMode::End);
        body.set_wrap_mode(pango::WrapMode::Word);
        body.set_wrap(true);
        body.set_lines(9);
        body.set_css_classes(&["caption"]);
        body.set_properties(&[
            ("margin-start", &12),
            ("margin-end", &12),
            ("margin-top", &12),
            ("margin-bottom", &12),
        ]);
        body.set_xalign(0.0);
        grid_box.append(&body);
        self.imp().body.set(body).unwrap();
        self.set_css_classes(&["card", "activatable"]);
        self.set_child(Some(&grid_box));

        self.set_properties(&[
            ("height-request", &160),
            ("width-request", &99),
            ("margin-start", &12),
            ("margin-end", &12),
            ("margin-top", &12),
            ("margin-bottom", &12),
        ]);
        self.set_action_name(Some("navigation.push"));
        self.set_action_target(Some("text-view"));
    }

    pub fn update_title(&self, document_content: &str) {
        let mut document = document_content.split_terminator('\n');
        let document_title = if let Some(first_line) = document.next() {
            if first_line.is_empty() {
                "Untitled"
            } else {
                first_line
            }
        } else {
            "Untitled"
        };

        let title = self.imp().title.get().unwrap();
        title.set_label(document_title);
    }

    pub fn update_body(&self, document_content: &str) {
        let body = self.imp().body.get().unwrap();
        body.set_label(document_content);
    }

    pub fn update_preview(&self, document_name: &str) {
        let document_content = if let Some(user_data_dir) = dirs::data_dir() {
            let sentence_data_dir = user_data_dir.join("sentence");
            fs::create_dir_all(&sentence_data_dir).expect("Could not create Directory.");

            let document_file_name: String = document_name.to_string() + ".md";

            let file_path = sentence_data_dir.join(document_file_name);
            fs::read_to_string(file_path).unwrap()
        } else {
            panic!("Could not open document, no access to directory.");
        };
        self.update_title(document_content.as_ref());
        self.update_body(document_content.as_ref());
    }
}
