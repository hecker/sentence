/* application.rs
 *
 * Copyright 2023 Justus Hecker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.or1g/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::APP_ID;

use crate::config::VERSION;
use crate::SentenceWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct SentenceApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for SentenceApplication {
        const NAME: &'static str = "SentenceApplication";
        type Type = super::SentenceApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for SentenceApplication {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
            obj.set_accels_for_action("win.close", &["<Ctrl>w"]);
            obj.set_accels_for_action("text.open", &["<Ctrl>o"]);
            obj.set_accels_for_action("text.save", &["<Ctrl>s"]);
            obj.set_accels_for_action("text.reset", &["<Ctrl>r"]);
            obj.set_accels_for_action("win.important", &["<Ctrl>i"]);
            obj.set_accels_for_action("win.link", &["<Ctrl>k"]);
            obj.set_accels_for_action("text.format-normal", &["<Ctrl>n"]);
            obj.set_accels_for_action("text.format-list", &["<Ctrl>l"]);
            obj.set_accels_for_action("text.format-quote", &["<Ctrl>u"]);
            obj.set_accels_for_action("text.format-monospace", &["<Ctrl>m"]);
            obj.set_accels_for_action("text.format-header-1", &["<Ctrl>1"]);
            obj.set_accels_for_action("text.format-header-2", &["<Ctrl>2"]);
            obj.set_accels_for_action("text.format-header-3", &["<Ctrl>3"]);
            obj.set_accels_for_action("text.format-header-4", &["<Ctrl>4"]);
        }
    }

    impl ApplicationImpl for SentenceApplication {
        fn activate(&self) {
            let application = self.obj();
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = SentenceWindow::new(&*application);
                window.upcast()
            };

            window.present();
        }
    }

    impl GtkApplicationImpl for SentenceApplication {}
    impl AdwApplicationImpl for SentenceApplication {}
}

glib::wrapper! {
    pub struct SentenceApplication(ObjectSubclass<imp::SentenceApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl SentenceApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder()
            .property("application-id", application_id)
            .property("flags", flags)
            .build()
    }

    fn setup_gactions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();
        self.add_action_entries([quit_action, about_action]);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutDialog::builder()
            .application_name("Sentence")
            .application_icon(APP_ID)
            .developer_name("Justus Hecker")
            .version(VERSION)
            .developers(vec!["Justus Hecker"])
            .designers(vec!["Brage Fuglseth"])
            .copyright("© 2023 - 2024 Justus Hecker")
            .build();

        about.present(Some(&window));
    }
}
