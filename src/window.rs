/* window.rs
 *
 * Copyright 2023 Justus Hecker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use crate::document_grid_preview::DocumentGridPreview;
use crate::writer_text_view::WriterTextView;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gio::{ActionEntry, Settings};
use glib::{clone, closure_local};
use gtk::{gio, glib, GridView, ListItem, NoSelection, SignalListItemFactory, StringList};
use std::cell::OnceCell;
use std::fs;
use std::io::Error;
use std::time::UNIX_EPOCH;
use uuid::Uuid;

use crate::APP_ID;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/academy/mcw/Sentence/window.ui")]
    pub struct SentenceWindow {
        // Settings
        pub settings: OnceCell<Settings>,
        // Template widgets
        #[template_child]
        pub nav_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        pub text_page: TemplateChild<adw::NavigationPage>,
        #[template_child]
        pub format_selector_button: TemplateChild<gtk::MenuButton>,
        #[template_child]
        pub format_selector_popover: TemplateChild<gtk::Popover>,
        #[template_child]
        pub text_view: TemplateChild<WriterTextView>,
        #[template_child]
        pub grid_view: TemplateChild<GridView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SentenceWindow {
        const NAME: &'static str = "SentenceWindow";
        type Type = super::SentenceWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SentenceWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.document_names();
            obj.setup_settings();
            obj.load_window_size();
            obj.setup_gactions();

            obj.setup_grid_view();
            obj.update_title();
            obj.update_format_selector();

            self.text_view.buffer().connect_changed(clone!(
                #[weak]
                obj,
                move |_buffer| {
                    obj.update_title();
                    obj.update_format_selector();
                }
            ));

            self.text_view.connect_closure(
                "format-changed",
                false,
                closure_local!(
                    #[weak_allow_none]
                    obj,
                    move |_text_view: WriterTextView| {
                        obj.unwrap().update_format_selector();
                    }
                ),
            );

            self.text_view.buffer().connect_local(
                "notify::cursor-position",
                false,
                clone!(
                    #[weak]
                    obj,
                    #[upgrade_or]
                    None,
                    move |_| {
                        obj.update_format_selector();
                        None
                    }
                ),
            );

            self.nav_view.connect_local(
                "popped",
                false,
                clone!(
                    #[weak]
                    obj,
                    #[upgrade_or]
                    None,
                    move |_navigation_view| {
                        obj.setup_grid_view();
                        None
                    }
                ),
            );
        }
    }
    impl WidgetImpl for SentenceWindow {}
    impl WindowImpl for SentenceWindow {
        fn close_request(&self) -> glib::Propagation {
            self.obj()
                .save_window_size()
                .expect("Could not save window size.");
            glib::Propagation::Proceed
        }
    }
    impl ApplicationWindowImpl for SentenceWindow {}
    impl AdwApplicationWindowImpl for SentenceWindow {}
}

glib::wrapper! {
    pub struct SentenceWindow(ObjectSubclass<imp::SentenceWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl SentenceWindow {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }

    fn setup_gactions(&self) {
        let close_action = ActionEntry::builder("close")
            .activate(move |win: &Self, _, _| win.close())
            .build();
        let new_document_action = ActionEntry::builder("new-document")
            .activate(move |win: &Self, _, _| {
                win.new_document().unwrap_err();
            })
            .build();
        self.add_action_entries([close_action, new_document_action]);

        let text_view_actions = self.imp().text_view.setup_gactions();
        self.insert_action_group("text", Some(&text_view_actions));
    }

    // Window size functions from gtk-rs book by Julian Hofer.
    fn setup_settings(&self) {
        let settings = Settings::new(APP_ID);
        self.imp()
            .settings
            .set(settings)
            .expect("`settings` should not be set before calling `setup_settings`.");
    }

    fn settings(&self) -> &Settings {
        self.imp()
            .settings
            .get()
            .expect("`settings` should be set in `setup_settings`.")
    }

    pub fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let size = self.default_size();

        self.settings().set_int("window-width", size.0)?;
        self.settings().set_int("window-height", size.1)?;
        self.settings()
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let width = self.settings().int("window-width");
        let height = self.settings().int("window-height");
        let is_maximized = self.settings().boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    fn new_document(&self) -> Result<(), Error> {
        let id = Uuid::new_v4().to_string();
        if let Some(user_data_dir) = dirs::data_dir() {
            let sentence_data_dir = user_data_dir.join("sentence");
            let document_file_name = format!("{}.md", id);
            let document_path = sentence_data_dir.join(document_file_name);
            fs::File::create(document_path)?;
        } else {
            return Err(Error::other("Could not create document."));
        }
        self.imp().text_view.set_document(id);
        self.setup_grid_view();
        Ok(())
    }

    fn update_title(&self) {
        let title = self.imp().text_view.title();
        self.imp().text_page.set_title(title.as_ref());
    }

    pub fn update_format_selector(&self) {
        let text_view = &self.imp().text_view;
        let text_type = text_view.paragraph_format_mark();
        self.imp().format_selector_button.set_label(text_type)
    }

    fn setup_grid_view(&self) {
        let document_names = self.document_names();

        if document_names.is_empty() {
            self.new_document()
                .expect("Can not create document, further running can create an infinite loop.");
            return;
        }

        let document_names: Vec<String> = document_names.iter().map(|s| s.to_string()).collect();
        let document_names: Vec<&str> = document_names.iter().map(|s| s.as_str()).collect();

        let model: StringList = StringList::new(document_names.as_slice());

        let factory = SignalListItemFactory::new();
        factory.connect_setup(clone!(
            #[weak(rename_to = obj)]
            self,
            #[weak]
            model,
            move |_, list_item| {
                let list_item = list_item
                    .downcast_ref::<ListItem>()
                    .expect("Needs to be ListItem");
                let button = DocumentGridPreview::new();
                button.connect_clicked(clone!(
                    #[weak]
                    obj,
                    #[weak]
                    list_item,
                    move |_| {
                        let document_name = model.string(list_item.position()).unwrap().to_string();
                        obj.imp().text_view.set_document(document_name);
                    }
                ));
                list_item.set_child(Some(&button));
                list_item.set_activatable(false);
            }
        ));
        factory.connect_bind(clone!(
            #[weak]
            model,
            move |_, list_item| {
                let list_item = list_item
                    .downcast_ref::<ListItem>()
                    .expect("Needs to be ListItem");
                let button = list_item.child().unwrap();
                let button = button
                    .downcast_ref::<DocumentGridPreview>()
                    .expect("Needs to be a DocumentGridPreview");
                if let Some(document_name) = model.string(list_item.position()) {
                    button.update_preview(document_name.as_ref());
                }
            }
        ));

        let selection_model = NoSelection::new(Some(model));

        let grid_view = &self.imp().grid_view;
        grid_view.set_model(Some(&selection_model));
        grid_view.set_factory(Some(&factory));

        // BUG: Somehow the class "background" has to be set via code.
        grid_view.set_css_classes(&["background"]);
    }

    fn document_names(&self) -> Vec<String> {
        let mut document_names: Vec<String> = Vec::new();
        if let Some(user_data_dir) = dirs::data_dir() {
            let sentence_data_dir = user_data_dir.join("sentence");
            fs::create_dir_all(&sentence_data_dir).expect("Could not create Directory.");
            let mut files: Vec<_> = fs::read_dir(sentence_data_dir)
                .unwrap()
                .filter_map(|entry| entry.ok())
                .collect();

            files.sort_by(|a, b| {
                let a_time = fs::metadata(a.path()).unwrap().modified().unwrap();
                let b_time = fs::metadata(b.path()).unwrap().modified().unwrap();
                a_time
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .cmp(&b_time.duration_since(UNIX_EPOCH).unwrap())
            });

            for file in files.into_iter().rev() {
                let file_name = file.file_name().into_string().unwrap();
                if file_name.ends_with(".md") {
                    let document_name = file_name[..file_name.len() - 3].to_string();
                    document_names.push(document_name);
                }
            }
        }
        document_names
    }
}
