/* writer_text_view.rs
 *
 * Copyright 2023 Justus Hecker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use adw::subclass::prelude::ObjectSubclassIsExt;
use gio::SimpleActionGroup;
use glib::subclass::Signal;
use glib::{clone, Object};
use gtk::{gio, glib, TextBuffer, TextIter, TextMark};
use once_cell::sync::Lazy;
use std::fs;

use std::sync::{Arc, Mutex};

// Lorem impsum from Sentence Mockup refined
const LOREM_IPSUM_MARKDOWN: &str = "# Lorem Ipsum
## Lorem Ipsum
### Lorem Ipsum
#### Lorem Ipsum
Cause this is a test
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore disputandum putant. Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae et terroribus cupiditatibusque detractis et omnium falsarum opinionum temeritate derepta certissimam se nobis ducem praebeat ad voluptatem. Sapientia enim est a Chrysippo praetermissum in.
> Omnes, fatendum est summum esse bonum iucunde vivere. Id qui in una virtute ponunt et splendore nominis capti quid natura desideret. Tum vero, si stabilem scientiam rerum tenebimus, servata illa, quae quasi titillaret sensus, ut ita dicam, et ad corpus nihil referatur.
    Ibidem homo acutus, cum illud ocurreret, si omnia deorsus e regione ferrentur et, ut dixi, ad lineam, numquam fore ut atomus altera alteram posset attingere itaque attulit rem commenticiam: declinare dixit atomum perpaulum, quo nihil posset fieri minus; ita effici complexiones et copulationes et adhaesiones atomorum inter se, ex quo efficiantur ea, quae sensum moveat, nulla successerit, eoque intellegi potest quanta voluptas sit non dolere. Sed ut perspiciatis.";

const PREFIXES_TO_TAGS_AND_NAMES: [(&str, &str, &str); 8] = [
    ("# ", "header-1", "Header 1"),
    ("## ", "header-2", "Header 2"),
    ("### ", "header-3", "Header 3"),
    ("#### ", "header-4", "Header 4"),
    ("- ", "list", "List"),
    ("> ", "quote", "Quote"),
    ("    ", "monospace", "Monospace"),
    ("", "normal", "Normal"),
];

mod imp {
    use super::*;
    use adw::subclass::prelude::*;
    use gtk::glib;

    #[derive(Default)]
    pub struct WriterTextView {
        pub line_format_marks: Arc<Mutex<Vec<(String, TextMark)>>>,
        pub document_name: Arc<Mutex<String>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WriterTextView {
        const NAME: &'static str = "SentenceWriterTextView";
        type Type = super::WriterTextView;
        type ParentType = gtk::TextView;
    }

    impl ObjectImpl for WriterTextView {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("format-changed").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            let obj = self.obj();
            // BUG: '.view' is not overwritten with '.background' in Blueprint. Code works.
            obj.set_css_classes(&["background"]);
            obj.open();

            obj.buffer().connect_changed(clone!(
                #[weak]
                obj,
                move |_| {
                    glib::spawn_future_local(clone!(
                        #[weak]
                        obj,
                        async move {
                            obj.save().await;
                            obj.update_marks();
                        }
                    ));
                }
            ));
        }
    }

    impl WidgetImpl for WriterTextView {}

    impl TextViewImpl for WriterTextView {}
}

glib::wrapper! {
    pub struct WriterTextView(ObjectSubclass<imp::WriterTextView>)
        @extends gtk::TextView, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Scrollable, gio::ActionMap;
}

impl Default for WriterTextView {
    fn default() -> Self {
        Self::new()
    }
}

impl WriterTextView {
    // --------------------------------- Setup --------------------------------- //

    pub fn new() -> Self {
        Object::builder().build()
    }

    pub fn setup_gactions(&self) -> SimpleActionGroup {
        let text_view = self;
        let reset_action = gio::ActionEntry::builder("reset")
            .activate(clone!(
                #[weak]
                text_view,
                move |_, _, _| text_view.reset(LOREM_IPSUM_MARKDOWN)
            ))
            .build();
        let important_action = gio::ActionEntry::builder("important")
            .activate(clone!(
                #[weak]
                text_view,
                move |_, _, _| text_view.important_text()
            ))
            .build();

        let actions = SimpleActionGroup::new();
        actions.add_action_entries([reset_action, important_action]);

        for (_, option, _) in PREFIXES_TO_TAGS_AND_NAMES {
            let action = gio::ActionEntry::builder(&format!("format-{}", option))
                .activate(clone!(
                    #[weak]
                    text_view,
                    move |_, _, _| text_view.toggle_paragraph_format(option)
                ))
                .build();
            actions.add_action_entries([action]);
        }

        actions
    }

    pub fn setup_styles(&self) {
        let buffer = self.buffer();

        // Default text styling
        self.set_pixels_above_lines(6);
        self.set_pixels_below_lines(6);

        // TODO: Not pretty
        if buffer.tag_table().size() != 0 {
            return;
        }

        buffer.create_tag(Some("important"), &[("weight", &700_i32.to_value())]);

        // Paragraph wise tags
        buffer.create_tag(
            Some("list"),
            &[
                ("size-points", &11_f64),
                ("pixels-below-lines", &6_i32),
                ("pixels-above-lines", &6_i32),
            ],
        );
        buffer.create_tag(
            Some("quote"),
            &[
                ("size-points", &12_f64),
                ("family", &"Times"),
                ("left-margin", &30_i32),
                ("pixels-below-lines", &6_i32),
                ("pixels-above-lines", &6_i32),
            ],
        );
        buffer.create_tag(
            Some("monospace"),
            &[
                ("family", &"Source Code Pro"),
                ("size-points", &10_f64),
                ("pixels-below-lines", &6_i32),
                ("pixels-above-lines", &6_i32),
            ],
        );

        buffer.create_tag(
            Some("header-1"),
            &[
                ("weight", &800_i32),
                ("size-points", &20_f64),
                ("pixels-below-lines", &10_i32),
                ("pixels-above-lines", &40_i32),
            ],
        );
        buffer.create_tag(
            Some("header-2"),
            &[
                ("weight", &700_i32),
                ("size-points", &16_f64),
                ("pixels-below-lines", &7_i32),
                ("pixels-above-lines", &20_i32),
            ],
        );
        buffer.create_tag(
            Some("header-3"),
            &[
                ("weight", &700_i32),
                ("size-points", &14_f64),
                ("pixels-below-lines", &7_i32),
                ("pixels-above-lines", &15_i32),
            ],
        );
        buffer.create_tag(
            Some("header-4"),
            &[
                ("weight", &700_i32),
                ("size-points", &11_f64),
                ("pixels-above-lines", &10_i32),
                ("pixels-below-lines", &6_i32),
            ],
        );
    }

    // --------------------------------- Document change --------------------------------- //

    pub fn set_document(&self, document_name: String) {
        let document_name_cloned = Arc::clone(&self.imp().document_name);
        {
            let mut name = document_name_cloned.lock().unwrap();
            *name = document_name;
        }
        self.open();
    }

    pub fn open(&self) {
        let markdown_text = if let Some(user_data_dir) = dirs::data_dir() {
            let sentence_data_dir = user_data_dir.join("sentence");
            fs::create_dir_all(&sentence_data_dir).expect("Could not create Directory.");

            let document_name_cloned = Arc::clone(&self.imp().document_name);
            let document_name = document_name_cloned.lock().unwrap();
            let document_file_name: String = document_name.clone() + ".md";

            let file_path = sentence_data_dir.join(document_file_name);
            let file = fs::read_to_string(file_path);
            match file {
                Ok(markdown_text) => markdown_text,
                Err(error) => format!("# Document could not be read:\n    {error}").to_string(),
            }
        } else {
            eprintln!("Could not open document, no access to directory.");
            "# Document could not be read:\n    No access to directory".to_string()
        };
        self.reset(&markdown_text);
    }

    async fn save(&self) {
        let markdown_text = self.convert_to_markdown();
        if let Some(user_data_dir) = dirs::data_dir() {
            let sentence_data_dir = user_data_dir.join("sentence");
            fs::create_dir_all(&sentence_data_dir).expect("Could not create Directory.");

            let document_name_cloned = Arc::clone(&self.imp().document_name);
            let document_name = document_name_cloned.lock().unwrap();
            let document_file_name: String = document_name.clone() + ".md";

            let file_path = sentence_data_dir.join(document_file_name);
            if let Err(error) = fs::write(file_path, markdown_text) {
                eprintln!("Could not save: {error}");
            }
        } else {
            eprintln!("Could not save document, no access to directory.");
        }
    }

    pub fn reset(&self, markdown_text: &str) {
        let buffer = self.buffer();
        buffer.set_text("");
        self.setup_styles();

        let markdown_lines = markdown_text.split_terminator('\n');

        buffer.begin_irreversible_action();
        for (line_number, markdown_line) in markdown_lines.clone().enumerate() {
            let (tag_name, prefix) = if markdown_line.starts_with("# ") {
                (Some("header-1"), "# ")
            } else if markdown_line.starts_with("## ") {
                (Some("header-2"), "## ")
            } else if markdown_line.starts_with("### ") {
                (Some("header-3"), "### ")
            } else if markdown_line.starts_with("#### ") {
                (Some("header-4"), "#### ")
            } else if markdown_line.starts_with("- ") {
                (Some("list"), "- ")
            } else if markdown_line.starts_with("> ") {
                (Some("quote"), "> ")
            } else if markdown_line.starts_with("    ") {
                (Some("monospace"), "    ")
            } else {
                (None, "")
            };

            let line = markdown_line.strip_prefix(prefix).unwrap();
            let formatted_line =
                if line_number != markdown_lines.clone().collect::<Vec<_>>().len() - 1 {
                    format!("{line}\n")
                } else {
                    line.to_string()
                };

            buffer.insert(&mut buffer.end_iter(), &formatted_line);
            if let Some(name) = tag_name {
                self.set_format_mark_in_line(name, line_number as i32);
            } else {
                self.set_format_mark_in_line("normal", line_number as i32);
            }
        }
        self.update_tags_by_marks();
        buffer.end_irreversible_action();
    }

    // --------------------------------- Auxillary --------------------------------- //

    fn line_end_iter(&self, buffer: &TextBuffer, line_number: i32) -> TextIter {
        if let Some(start_next_line) = buffer.iter_at_line(line_number + 1) {
            buffer.iter_at_offset(start_next_line.offset() - 1)
        } else {
            buffer.end_iter()
        }
    }

    pub fn line_as_string(&self, line_number: i32) -> String {
        let buffer = self.buffer();
        let start = buffer.iter_at_line(line_number).unwrap();
        let end = self.line_end_iter(&buffer, line_number);

        buffer.text(&start, &end, false).to_string()
    }

    fn selection_iters(&self) -> (TextIter, TextIter) {
        let buffer = self.buffer();

        if let Some((selection_start, selection_end)) = buffer.selection_bounds() {
            let start = buffer.iter_at_line(selection_start.line()).unwrap();
            let end = self.line_end_iter(&buffer, selection_end.line());
            (start, end)
        } else {
            let line_number = buffer.iter_at_mark(&buffer.get_insert()).line();
            self.line_iters(line_number)
        }
    }

    fn line_iters(&self, line_number: i32) -> (TextIter, TextIter) {
        let buffer = self.buffer();

        let start = buffer.iter_at_line(line_number).unwrap();
        let end = self.line_end_iter(&buffer, line_number);

        (start, end)
    }

    fn convert_to_markdown(&self) -> String {
        let buffer = self.buffer();
        let tag_table = buffer.tag_table();
        let (start, end) = buffer.bounds();
        let text = buffer.text(&start, &end, false);
        let lines = text.split('\n');
        let mut markdown_text = "".to_string();

        for (line_number, line) in lines.enumerate() {
            let start = buffer.iter_at_line(line_number as i32).unwrap();
            let mut found_prefix = "";
            for (prefix, tag_name, _) in PREFIXES_TO_TAGS_AND_NAMES {
                if tag_name != "normal" {
                    let tag = tag_table.lookup(tag_name).expect(tag_name);
                    if start.has_tag(&tag) {
                        found_prefix = prefix;
                    }
                }
            }
            markdown_text.push_str(found_prefix);
            markdown_text.push_str(line);
            markdown_text.push('\n');
        }
        markdown_text.to_string()
    }

    pub fn title(&self) -> String {
        let first_line_text = self.line_as_string(0);
        if first_line_text.is_empty() {
            "New Document".to_string()
        } else {
            first_line_text
        }
    }

    // --------------------------------- Format --------------------------------- //

    fn toggle_paragraph_format(&self, toggle_format_option: &str) {
        let (start, end) = self.selection_iters();
        let mut format_option = toggle_format_option;
        if self.has_paragraph_format_mark(toggle_format_option, start.line()) {
            format_option = "normal";
        }
        self.set_format_mark_in_lines(format_option, start.line(), end.line());
        self.update_tags_by_marks();
        self.emit_by_name::<()>("format-changed", &[]);
    }

    pub fn paragraph_format_mark(&self) -> &str {
        let mut right_name = "Normal";
        let (start, _) = self.selection_iters();
        let line_number = start.line();
        self.line_format_mark(line_number);
        for (_, tag_name, name) in PREFIXES_TO_TAGS_AND_NAMES {
            if self.has_paragraph_format_mark(tag_name, line_number) {
                right_name = name
            }
        }
        right_name
    }

    fn line_format_mark(&self, line_number: i32) -> Option<String> {
        let buffer = self.buffer();
        let line_format_marks = Arc::clone(&self.imp().line_format_marks);
        for (format_name, mark) in line_format_marks.lock().unwrap().iter() {
            let mark_line_number = buffer.iter_at_mark(mark).line();
            if mark_line_number == line_number {
                return Some(format_name.to_string());
            }
        }
        None
    }

    fn has_paragraph_format_mark(&self, format_test: &str, line_number: i32) -> bool {
        if let Some(format) = self.line_format_mark(line_number) {
            format_test == format
        } else {
            false
        }
    }

    fn important_text(&self) {
        let buffer = self.buffer();
        if let Some((start, end)) = buffer.selection_bounds() {
            let tag = buffer
                .tag_table()
                .lookup("important")
                .expect("Tag name was not found");
            if start.has_tag(&tag) {
                buffer.remove_tag_by_name("important", &start, &end);
            } else {
                buffer.apply_tag_by_name("important", &start, &end);
            }
        } else {
            let insert_mark = buffer.get_insert();
            let insert_iter = buffer.iter_at_mark(&insert_mark);
            let bold_mark = buffer.create_mark(None, &insert_iter, false);
            buffer.apply_tag_by_name("important", &insert_iter, &buffer.iter_at_mark(&bold_mark));
        }
    }

    // --------------------------------- Tags --------------------------------- //

    fn delete_paragraph_format_tags(&self, line_number: i32) {
        let buffer = self.buffer();
        let (start, end) = self.line_iters(line_number);

        buffer.remove_all_tags(&start, &end);
    }

    // --------------------------------- Marks --------------------------------- //

    fn set_format_mark_in_line(&self, format_name: &str, line_number: i32) {
        let buffer = self.buffer();
        let line_format_marks_cloned = Arc::clone(&self.imp().line_format_marks);
        let mut line_format_marks = line_format_marks_cloned.lock().unwrap();
        for (i, (_, mark)) in line_format_marks.iter().enumerate() {
            if self.line_at_mark(mark) == line_number {
                line_format_marks[i] = (format_name.to_string(), mark.clone());
                return;
            }
        }
        if let Some(start_iter) = buffer.iter_at_line(line_number) {
            let mark = buffer.create_mark(None, &start_iter, true);
            line_format_marks.push((format_name.to_string(), mark));
        }
    }

    fn set_format_mark_in_lines(&self, format_name: &str, start_line: i32, end_line: i32) {
        for line in start_line..=end_line {
            self.set_format_mark_in_line(format_name, line);
        }
    }

    fn line_at_mark(&self, mark: &TextMark) -> i32 {
        let buffer = self.buffer();
        let iter = buffer.iter_at_mark(mark);
        iter.line()
    }

    fn update_tags_by_marks(&self) {
        let buffer = self.buffer();
        let line_format_marks_cloned = Arc::clone(&self.imp().line_format_marks);
        for (format_name, mark) in line_format_marks_cloned.lock().unwrap().iter() {
            let (start, end) = self.line_iters(self.line_at_mark(mark));
            self.delete_paragraph_format_tags(self.line_at_mark(mark));
            if format_name != "normal" {
                buffer.apply_tag_by_name(format_name, &start, &end);
            }
        }
    }

    fn remove_duplicate_and_refill_marks(&self) {
        let buffer = self.buffer();
        let max_line = buffer.end_iter().line();
        let line_format_marks_cloned = Arc::clone(&self.imp().line_format_marks);
        for line in 0..=max_line {
            let mut flag_first = true;
            let mut line_format_marks_to_remove = Vec::new();

            for (i, (_, mark)) in line_format_marks_cloned.lock().unwrap().iter().enumerate() {
                if self.line_at_mark(mark) == line {
                    if flag_first {
                        flag_first = false;
                        buffer.move_mark(mark, &buffer.iter_at_line(line).unwrap());
                    } else {
                        buffer.delete_mark(mark);
                        line_format_marks_to_remove.push(i);
                    }
                }
            }

            if flag_first {
                self.set_format_mark_in_line("normal", line);
            }

            line_format_marks_to_remove.sort_unstable();

            let mut line_format_marks = line_format_marks_cloned.lock().unwrap();
            for &i in line_format_marks_to_remove.iter().rev() {
                line_format_marks.remove(i);
            }
        }
    }

    fn update_marks(&self) {
        self.remove_duplicate_and_refill_marks();
        self.update_tags_by_marks();
    }
}
